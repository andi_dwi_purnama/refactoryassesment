const data = require('./data/user.json')
const queryJson = require('query-json');

const findRecursive = require('simple-object-query').find;
const searchRecursive = require('simple-object-query').search;
const { isEmpty, isArray, has, isString, contains, where } = require('underscore');

// 1. Find users who doesn't have any phone numbers.
const resultPhone = findRecursive(data, {'profile.phones': isEmpty});
console.log('Users who doesn\'t have any phone numbers: ');
console.log(resultPhone);

// 2. Find users who have article.
const resultHaveArticle = findRecursive(data, {'articles': !isEmpty});
console.log('Users who have articles: ');
console.log(resultHaveArticle);

// 3. Find users who have "annis" on their name.
const regex = new RegExp('annis', 'i');
const result = queryJson.search(data, regex);
const resultData = result.map((item) => {
    return data[item[0]];
})
console.log('Users who have "annis" on their name: ');
console.log(resultData);

// 4. Find users who have articles on year 2020.
const resultArticle2020 = searchRecursive({
    source: data,
    query: {
        'articles.published_at': /2020/
    }
});
console.log('Users who have articles on year 2020: ');
const resultDataArticle2020 = resultArticle2020.map((item) => {
    return item.target
})

console.log(resultDataArticle2020);

// 5. Find users who are born on 1986.
const resultBorn86 = searchRecursive({
    source: data,
    query: {
        'profile.birthday': /1986/
    }
});
console.log('Users who are born on 1986: ');
const resultData1986 = resultBorn86.map((item) => {
    return item.target
})

console.log(resultData1986);

// 6. Find articles that contain "tips" on the title
const regexTips = new RegExp('tips', 'i');
const resultArticle = queryJson.search(data, regexTips);
const resultDataArticle = resultArticle.map((item) => {
    return data[item[0]];
})

console.log('Articles that contain "tips" on the title:');
console.log(resultDataArticle);

