# CodeDebugging

Fix error:

* File .env tidak ada, maka buatlah file .env dengan mengganti nama file env dan isikan credential oauth github
* Pada file config/index.js `const envFound = dotenv.config();` ditaruh setelah pemanggilan modul `dotenv` agar config .env terbaca di object config = {}
* Destructure config di app.js menjadi: 

        const { config } = require("./src/config");
* Ada typo di file `authService.js`, kurang `s` di `modul.export`, yang benar adalah `module.exports`
* Destructure object `config` di file auth `authService` menjadi:

        const { config } = require("../config");
* Destructure object `config` di file auth `authCallbackService.js` menjadi:

        const { config } = require("../config");
* Ada typo `resp` di file `authCallbackService.js`, seharusnya `res`, diperbaiki menjadi:

        .then((res) => res.data["accessToken"])
* Kurang kurung kurawal di modul.exports file `userInfoService.js`, diperbaiki menjadi:
        module.exports = { getUserInfo }
* 