const axios = require("axios");
const {config} = require("../config");

function getUserInfo(token) {
  try {
    const ax = axios({
      method: "get",
      url: `${config.apiUrl}/users`,
      headers: {
        Authorization: "token " + token,
      },
    }).then((response) => {
      return response.data;
    });

    console.log(ax);
  } catch (error) {
    
  }
}

module.exports = { getUserInfo: getUserInfo }
