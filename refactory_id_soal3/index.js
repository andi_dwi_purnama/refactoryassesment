const data = require('./data/furniture.json')
const _ = require('underscore')
const findRecursive = require('simple-object-query').find;
const { keys, values } = require('underscore');


// 1. Find items in Meeting Room.
const resultMeeting = findRecursive(data, { 'placement.name': 'Meeting Room' });
console.log('All items in Meeting Room: ');
const resultDataMeeting = resultMeeting.map((item) => {
    return item.name;
})
console.log(resultDataMeeting);

// 2. Find all electronic devices.
const result = _.where(data, {type: 'electronic'});
console.log('All electronic devices: ');
const resultData = result.map((item) => {
    return item.name;
})
console.log(resultData);

// 3. Find all furnitures.
const resultFurniture = _.where(data, {type: 'furniture'});
console.log('All furnitures: ');
const resultDataFurniture = resultFurniture.map((item) => {
    return item.name;
})
console.log(resultDataFurniture);


// 4. Find all items was purchased at 16 Januari 2020.
const timeStamp = Date.parse('2020-01-16') / 1000; 

const resultPurchased = _.where(data, {purchased_at: timeStamp});
console.log('All items was purchased at 16 Januari 2020: ');
const resultDataPurchased = resultPurchased.map((item) => {
    return item.name;
})
console.log(resultDataPurchased == '' ? 'No item found' : resultDataPurchased);

// 5. Find all items with brown color.
const resultBrownColour = findRecursive(data, {tags: /brown/});
console.log('All items with brown color: ');
const resultDataBrown = resultBrownColour.map((item) => {
    return item.name;
})
console.log(resultDataBrown);