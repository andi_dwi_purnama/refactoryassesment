const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const schedule = require('node-schedule');
const fs = require('fs')
const cryptoRandomString = require('crypto-random-string')
const { counterPost } = require('./components')
const { postEvery5Minute } = require('./components/ApiCaller')


const app = express();

const router = express.Router();

router.post('/post_counter', counterPost);

//we are defining a new parameter called host
logger.token('host', function(req, res) {
  return req.hostname;
});

logger.token('timestamp', function(req, res) {
  const event = new Date();
  const dateTime = event.toString();
  return `[${dateTime}]`
});

app.use(logger(':timestamp Success: :method :host - :response-time ms', {
  stream: fs.createWriteStream('./server.log', {flags: 'a'})
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

schedule.scheduleJob('*/5 * * * *', postEvery5Minute({counter: cryptoRandomString({length: 2, type: 'numeric'})}))

module.exports = app;
