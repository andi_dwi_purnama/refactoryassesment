'use strict'

const axios = require('axios')
const cryptoRandomString = require('crypto-random-string')


const postEvery5Minute = async (inputData) => {
    try {
        const endPoint = `/post_counter`;

        const instance = axios.create({
            baseURL: 'http://localhost:3000',
            timeout: 3000,
            headers: {
                'X-RANDOM': cryptoRandomString({length: 6, type: 'distinguishable'})
            },
        });

        const data = await instance.post(endPoint, inputData);

        console.log(data, 'post');

        return data;
    } catch (error) {
        console.log(error);
    }
};

module.exports = { postEvery5Minute }